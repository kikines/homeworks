function createNewUser(){
    this.firstName = prompt("Enter first name");
    this.lastName = prompt("Enter last name");
    this.getLogin = function(firstName,lastName){
        let login = firstName.slice(0,1) + "_" + lastName;
        return login.toLowerCase();
    } 
    this.login = this.getLogin(this.firstName, this.lastName);
}
let user = new CreateNewUser();
console.log(user);
