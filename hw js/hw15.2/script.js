let pause = document.createElement('button');
let start = document.createElement('button');
let clear = document.createElement('button');

clear.innerText = 'clear';
start.innerText = 'start';
pause.innerText = 'pause';

document.body.appendChild(start);
document.body.appendChild(clear);

let counter = 1;
let counterMili = 1;
let counterMin = 1;

let clockArrow = document.getElementById('secArrow');
let milisecArrow = document.getElementById('milisecArrow');
let minuteArrow = document.getElementById('minArrow');

start.addEventListener('click', () => {
  start.remove();
  document.body.insertBefore(pause,clear);

  let int = setInterval( () => {
    clockArrow.style.transform = `rotateZ(${6*counter}deg)`;
    counter++;
  },1000)

  let int1 = setInterval(() => {
    milisecArrow.style.transform = `rotateZ(${360/100*counterMili}deg)`
    counterMili++;
  },10)

  let int2 = setInterval(() => {
    minuteArrow.style.transform = `rotateZ(${6*counterMin}deg)`
    counterMin++
  },60000)

  pause.addEventListener('click', () => {
    pause.remove();
    document.body.insertBefore(start,clear);
    clearInterval(int);
    clearInterval(int1);
    clearInterval(int2);
  })

})
clear.addEventListener('click', ()=> {
  clockArrow.style.transform = `rotateZ(0deg)`;
  milisecArrow.style.transform = `rotateZ(0deg)`
  minuteArrow.style.transform = `rotateZ(0deg)`
  counter = 1;
  counterMili = 1;
  counterMin = 1;
})