let workSpeed = [1,1,1,1,1];
let taskPoints = [10,10,10,10,10];
let deadLine = new Date(2019,0,1);
let deadLine1 = new Date(2018,11,9);
calculate(workSpeed,taskPoints,deadLine);
calculate(workSpeed,taskPoints,deadLine1);

function calculate(workSpeed,taskPoints,deadLine){
let now = new Date();
let totalSpeed = 0; 
let totalPoints = 0; 
for(let elem of workSpeed){
totalSpeed += elem; // общая производительность всех членов команды в день
}
for(let elem of taskPoints){
    totalPoints += elem; // общая сумма стори поинтов всех задач
    }

let hoursNeeded = totalPoints/totalSpeed * 8; // нужное количество рабочих часов для выполнения всех задач
let dateArr = [];
for(let i = 0; i <= Math.ceil((deadLine.getTime() - now.getTime()) / (1000 * 3600 * 24)); i++){
    dateArr[i] = new Date(now.getFullYear(), now.getMonth(), now.getDate()+i); // создаю масив дат от текущего дня до дедлайна
}

for(let elem of dateArr){
    if(elem.getDay() != 6 && elem.getDay() != 0){
        hoursNeeded -= 8; // если день рабочий, то отнимаю от количества часов для выполнения всей работы 8 часов
    }
}
if(hoursNeeded <= 0)
alert(`Все задачи будут успешно выполнены за ${Math.floor(hoursNeeded/-8)} рабочих дней до наступления дедлайна!` ); // лишние рабочие часы округляю и перевожу в количество рабочих дней до дедлайна
else{
alert(`Команде разработчиков придется потратить дополнительно ${hoursNeeded} рабочих часов после дедлайна, чтобы выполнить все задачи в беклоге`)
}

}