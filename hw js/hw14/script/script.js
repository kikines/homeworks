let calcScreen = document.getElementById('screen');
let display = document.getElementsByClassName('display')[0];
let memoryIndicator = document.createElement('div');
memoryIndicator.innerText = 'M';
memoryIndicator.style = 'position: absolute; height: 10px; width: 10px; top :3px; left: 5px'

let firstPart = ''; //first number + operator
let operatorsCheck = 0; // opportunity to click a lot on any operators before inputing second number,the last will be selected
let memory = 0;
let memoryCounter = 0;
let clearScreen = false; // clears screen before entering another number

document.addEventListener('keypress',() => {
  if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode == 46) {
    addNum(String.fromCharCode(event.keyCode));
  } else if (event.keyCode == 42 || event.keyCode == 43 || event.keyCode == 45 || event.keyCode == 47) {
    addOperator(String.fromCharCode(event.keyCode));
  } else if (event.keyCode == 13) {
    calculate()
  }
})

/*functions*/

function addNum(num) {
  if(clearScreen == true){ // clears screen before entering another number
    calcScreen.value = null; 
    clearScreen = false;
  }

calcScreen.value = calcScreen.value + num;
operatorsCheck = 0;
}

function addOperator(operator) {
  operatorsCheck++

  if (operatorsCheck < 2) { // opportunity to click a lot on any operators before inputing second number,the last will be selected
    calculate();           //calculates expression by clicking on operator as well as by clicking on '='           
  }

  firstPart = calcScreen.value + operator;
  clearScreen = true;      
}

function calculate() {
  calcScreen.value = eval(firstPart + calcScreen.value);
  firstPart = '';
}

function totalClear() {
  calcScreen.value = null;
  firstPart = '';
}

function showMemoryIndicator() {
  if (memory != 0){
    display.appendChild(memoryIndicator);
  } else {
    memoryIndicator.remove();
  }
}

function plusMemory() {
  memory+=Number(calcScreen.value);
  memoryCounter = 0;
  showMemoryIndicator()
}

function minusMemory() {
  memory-=Number(calcScreen.value);
  memoryCounter = 0;
  showMemoryIndicator()
}

function showMemory() {
  calcScreen.value = memory;
  memoryCounter++

  if (memoryCounter == 2) {
    memory = 0;
  }
  
  showMemoryIndicator()
}




