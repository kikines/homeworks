let logo = document.getElementsByClassName('journey_logo')[0];
let welcome = document.getElementsByClassName('welcome')[0];
let signUp = document.getElementsByClassName('sign_up_block')[0];
let facebook = document.getElementsByClassName('facebook')[0];
let google = document.getElementsByClassName('google')[0];
let create = document.getElementsByClassName('create')[0];
let button = document.getElementById('btn');
let links = document.getElementsByClassName('green');
let square = document.getElementsByClassName('green_square')[0];

  if (localStorage.getItem('theme') == null) {
    localStorage.setItem('theme','day');
  } else if (localStorage.getItem('theme') == 'night') {
    changeToNight();
  }

button.addEventListener('click', () => {

  if (localStorage.getItem('theme') == 'day') {
    changeToNight();
  } else {
    changeToDay();
  }

})

function changeToDay(){
  
  logo.style.color = 'white';
  welcome.style.color = 'white';
  signUp.style.backgroundColor = 'white';
  facebook.style.backgroundColor = '#305db8';
  google.style.backgroundColor = '#df4a32';
  create.style.backgroundColor = '#3cb878';
  button.style.backgroundColor = '#3cb878'
  square.style.backgroundColor = '#3cb878';

  for (let element of links) {
    element.style.color = '#3cb878';
  }
  
  localStorage.setItem('theme', 'day'); 
}

function changeToNight() {

  logo.style.color = 'grey';
  welcome.style.color = 'grey';
  signUp.style.backgroundColor = 'grey';
  facebook.style.backgroundColor = 'rgba(57, 74, 228, 0.7)';
  google.style.backgroundColor = 'rgba(241, 90, 90, 0.7)';
  create.style.backgroundColor = 'rgb(40,40,40)';
  button.style.backgroundColor = 'rgba(0,0,0,0.6)'
  square.style.backgroundColor = 'rgba(0,0,0,0.6)';

  for (let element of links) {
    element.style.color = 'beige';
}

  localStorage.setItem('theme', 'night');
}



