let object = {
    name : "default",
    firstInsideObj : {
        name : "default",
        secondInsideObj : {
            name : "default"
        }
    },
    arr : [1,2,3,4,5]

}

function cloneObject (object){
    let clonedObject = {};
    for(let elem in object){
        if(typeof object[elem] == 'object'){
            clonedObject[elem] = cloneObject(object[elem]);
        }else{
            clonedObject[elem] = object[elem];
        }
    }
    
    return clonedObject;
}
let clonedObject = cloneObject(object);
console.log(clonedObject);

//проверка
// let clonedObject = cloneObject(object);
// object.name = "changed";
// object.firstInsideObj.name = "changed";
// object.firstInsideObj.secondInsideObj.name = "changed";
//object.arr.splice(0,5,6,7,8,9,10);
// console.log(object);
// console.log(clonedObject);
